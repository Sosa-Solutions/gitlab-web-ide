// eslint-disable-next-line import/no-extraneous-dependencies
import {
  RemoteConfig,
  ClientOnlyConfig,
  AnyConfig,
  ConfigType,
  Message,
  WebIde,
} from '@gitlab/web-ide-types';

import { getIframeHtml } from './getIframeHtml';
import handleVSCodeTrackingMessage from './handleVSCodeTrackingMessage';
import { createUnloadPreventer, IUnloadPreventer } from './unloadPreventer';

export const createError = (msg: string) => new Error(`[gitlab-vscode] ${msg}`);

interface IHandleMessagesOptions {
  iframe: HTMLIFrameElement;
  config: AnyConfig;
  unloadPreventer: IUnloadPreventer;
}

const handleMessages = ({ iframe, config, unloadPreventer }: IHandleMessagesOptions) => {
  const { contentWindow } = iframe;

  if (!contentWindow) {
    throw createError('Could not find contentWindow for iframe.');
  }

  contentWindow.addEventListener('message', (e: MessageEvent<Message>) => {
    switch (e.data?.key) {
      case 'start-remote':
        config.handleStartRemote?.(e.data.params);
        break;
      case 'error':
        config.handleError?.(e.data.params);
        break;
      case 'close':
        config.handleClose?.();
        break;
      case 'prevent-unload':
        unloadPreventer.setShouldPrevent(e.data.params.shouldPrevent);
        break;
      case 'vscode-tracking':
        handleVSCodeTrackingMessage(e.data.params, config);
        break;
      case 'web-ide-tracking':
        config.handleTracking?.(e.data.params.event);
        break;
      default:
        break;
    }
  });
};

const waitForReady = (iframe: HTMLIFrameElement): Promise<void> => {
  const { contentWindow } = iframe;

  if (!contentWindow) {
    throw createError('Could not find contentWindow for iframe.');
  }

  return new Promise<void>(resolve => {
    const listener = (e: MessageEvent<Message>) => {
      if (e.data?.key === 'ready') {
        contentWindow.removeEventListener('message', listener);
        resolve();
      }
    };

    contentWindow.addEventListener('message', listener);
  });
};

const startAnyConfig = (el: Element, configType: ConfigType, config: AnyConfig): WebIde => {
  const iframe = document.createElement('iframe');
  Object.assign(iframe.style, {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    height: '100%',
    border: 'none',
    margin: 0,
    padding: 0,
  });
  el.appendChild(iframe);

  if (!iframe.contentWindow) {
    throw createError('Could not find contentWindow for iframe.');
  }

  iframe.contentWindow.document.open();
  iframe.contentWindow.document.write(getIframeHtml(configType, config));
  iframe.contentWindow.document.close();

  const unloadPreventer = createUnloadPreventer();
  handleMessages({ iframe, config, unloadPreventer });

  return {
    dispose() {
      iframe.remove();
      unloadPreventer.dispose();
    },
    ready: configType === 'client-only' ? waitForReady(iframe) : Promise.resolve(),
  };
};

export const start = (el: Element, config: ClientOnlyConfig) =>
  startAnyConfig(el, 'client-only', config);

export const startRemote = (el: Element, config: RemoteConfig) =>
  startAnyConfig(el, 'remote', config);
